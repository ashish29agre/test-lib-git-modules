package in.ashish29agre.utils;

public class StringUtils {
    private StringUtils() {
    }

    public static CharSequence replaceSpacesWithUnderscore(String string) {
        if (string != null) {
            if (string.length() == 0) {
                throw new IllegalArgumentException("Zero(0) length String encountered");
            } else {
                return string.replaceAll(" ", "_").toLowerCase();
            }
        } else {
            throw new IllegalArgumentException("String cannot be null");
        }
    }
}